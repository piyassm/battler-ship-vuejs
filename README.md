# battleship-vuejs

## Project setup
```
npm install
```

### Unit test
```
npm run test
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

Demo Here
\
https://battle-ship-vuejs.web.app/