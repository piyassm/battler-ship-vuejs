import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Attacker from '../views/Attacker.vue'
import Defender from '../views/Defender.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/attacker',
    name: 'Attacker',
    component: Attacker
  }, {
    path: '/defender',
    name: 'Defender',
    component: Defender
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
