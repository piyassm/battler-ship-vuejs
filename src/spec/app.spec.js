import Home from "../views/Home.vue";
import Attacker from "../views/Attacker.vue";
import Defender from "../views/Defender.vue";

it("Attacker data move default move is zero", () => {
  const defaultData = Attacker.data();
  expect(defaultData.move).toEqual(0);
});

it("Attacker data move miss default is zero", () => {
  const defaultData = Attacker.data();
  expect(defaultData.movemiss).toEqual(0);
});

it("Attacker message method check status board is correct", () => {
  const defaultData = Attacker.methods;
  expect(defaultData.isDamage({ hit: true, target: true })).toEqual(
    "app__boardDanger"
  );
  expect(defaultData.isDamage({ hit: true })).toEqual("app__boardDark");
});

it("Defender message method check status board is correct", () => {
  const defaultData = Attacker.methods;
  expect(defaultData.isDamage({ hit: true, target: true })).toEqual(
    "app__boardDanger"
  );
  expect(defaultData.isDamage({ hit: true })).toEqual("app__boardDark");
});

it("Defender data move default move is zero", () => {
  const defaultData = Defender.data();
  expect(defaultData.move).toEqual(0);
});

it("Defender data move miss default is zero", () => {
  const defaultData = Defender.data();
  expect(defaultData.movemiss).toEqual(0);
});

it("Defender message method check status player select board is correct", () => {
  const defaultData = Defender.methods;
  expect(defaultData.isSelect({ hit: true, target: true })).toEqual(
    "app__boardDanger"
  );
  expect(defaultData.isSelect({ hover: true, target: false })).toEqual(
    "app__boardHover"
  );
  expect(defaultData.isSelect({ hover: true, target: true })).toEqual(
    "app__boardDanger"
  );
  expect(defaultData.isSelect({ target: true })).toEqual("app__boardInfo");
});
